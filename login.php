<?php
session_start();
session_destroy();
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Dodolantani</title>

  <!-- CSS  -->
  <link href="css/icon.css" rel="stylesheet">
  <link href="css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <style type="text/css">  
  </style>
</head>
<body>
    <nav class="nav-border">
    <div class="teal nav-wrapper">
      <div class="container">

        <div class="row">
          <div class="col s3">
            <a href="#" class="brand-logo">Dodolantani</a>
            <a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>
          </div>


          <div class="col s2 offset-s7 hide-on-med-and-down">
            <ul class="right">
              <li><a href="index.php">Home</a></li>
            </ul>
          </div>
        </div>

        <ul class="side-nav" id="mobile-menu">
            <li><a href="../index.php">Home</a></li>
            <li><a href="../search.php">Search</a></li>
        </ul>

      </div>
    </div>
  </nav> 

  <br>
 <div class="container">
    <div class="section"> 

      <!--   Side bar   -->
      <div class="row" style="background-color:#f5f5f5;">
        <div class="col s12 m4">
          <br>
          <div class="icon-block">
            <h4>Login</h4>
            <?php
            if(isset($_GET['msg'])){
              echo $_GET['msg'];
            }else
            echo "<br>";
          ?>
            <br>
            <form action="func/logincheck.php" method="post">
              <div class="input-field">
                <input type="text" name="username">
                <label>Username</label>
              </div>
              <div class="input-field">
                <input type="password" name="password">
                <label>Password</label>
              </div>
              <div class="input-field">
                <input type="submit" class="btn right" value="login">
              </div>
            </form><br><br>
          </div>
          <br><br>
        </div>

      <div class="col s12 m7 offset-m1">
          <br><br>
          <div class="icon-block">
          <br>
            Kami adalah sebuah layanan online yang melayani penjual-belian barang dan bahan yang berhubungan dengan pertanian. Anda dapat melakukan pembelian maupun penjualan. Website ini dibuat dengan tujuan untuk memenuhi syarat tugas akhir.<br><br>
            <br>Belum memiliki akun Dodolantani?<br>
            Daftar segera untuk dapat menikmati berbagai manfaat Dodolantani!<br><br>
            <a href="registrasi.php" class="btn">Daftar!</a>
          </div>
          <br><br>
        </div>
        <br>
      </div>

    </div>
    </div>
    </div>

  <br><br>

  <footer class="page-footer teal darken-4">

    <div class="footer-copyright">
      <div class="container">
      By <a class="orange-text text-lighten-3" href="#">72130021</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="js/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  </body>
</html>
