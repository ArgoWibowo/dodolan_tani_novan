<?php
mysql_connect("localhost", "root", "");
mysql_select_db("iais_ukdw");
session_start();
if(isset($_SESSION['username'])){
  header("Location:../index.php");
}elseif(isset($_SESSION['regun'])){
  $un = $_SESSION['regun'];
  session_destroy();
  include "headers/headerguest.php";
}else{
  header("Location:../index.php");
}


?>


  <br>
 <div class="container">
    <div class="section"> 

      <!--   Side bar   -->
      <div class="row">
        <div class="col s12 m7 offset-m2" style="background-color:#f5f5f5;">
          <br>
          <div class="icon-block">
            <h4>Daftar Dodolantani</h4>
            <br>
            
            <form action="func/daftarakun.php" method="post" enctype="multipart/form-data">
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">ID User</label>
              </div>
              <div class="input-field col s12 m8">
                <input type="text" id="un" name="username" value="<?php echo $un; ?>" disabled>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Nama Anda</label>
              </div>
              <div class="input-field col s12 m8">
                <input type="text" name="nama" required>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">No. Telepon</label>
              </div>
              <div class="input-field col s12 m8">
                <input type="text" name="notel" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46' required>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Email</label>
              </div>
              <div class="input-field col s12 m8">
                <input type="email" name="email" required>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Jenis Kelamin</label>
              </div>
              <div class="input-field col s12 m8">
                <select name="jeniskelamin">
                  <option value="1" selected>Laki-laki</option>
                  <option value="2">Perempuan</option>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Tanggal Lahir</label>
              </div>
              <div class="input-field col s12 m8">
                <input type="text" name="tgllahir" class="datepicker">
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <textarea id="alamat" name="alamat" class="materialize-textarea"></textarea>
                <label for="alamat" class="black-text">Alamat</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Provinsi</label>
              </div>
              <div class="input-field col s12 m8">
                <select name='prov'>
                <option>Pilih Prov</option>
                <?php
                $provinsi=mysql_query("SELECT * from provinsi");
                while($prov=mysql_fetch_array($provinsi))
                {
                ?>
                      <option value="<?php echo $prov['Nama_Provinsi'];?>"><?php echo $prov['Nama_Provinsi'];?></option>
                <?php
                }
                ?>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Kabupaten</label>
              </div>
              <div class="input-field col s12 m8">
                <select name='kab'>
                <option>Pilih Kabupaten</option>
                <?php
                $provinsi=mysql_query("SELECT * from provinsi");
                while($prov=mysql_fetch_array($provinsi))
                {
                ?>
                  <optgroup label="<?php echo $prov['Nama_Provinsi'];?>">
                    <?php
                    $kabupaten=mysql_query("SELECT * from kabupaten WHERE Nama_Provinsi='$prov[Nama_Provinsi]'");
                    while($kab=mysql_fetch_array($kabupaten))
                    {
                    ?>
                      <option value="<?php echo $kab['Nama_Kabupaten'];?>"><?php echo $kab['Nama_Kabupaten'];?></option>
                    <?php  
                    }
                    ?>
                  </optgroup>
                <?php
                }
                ?>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Kecamatan</label>
              </div>
              <div class="input-field col s12 m8">
                <select name='kecamatan'>
                <option>Pilih Kecamatan</option>
                <?php
                $provinsi=mysql_query("SELECT * from provinsi order by Nama_Provinsi");
                while($prov=mysql_fetch_array($provinsi))
                {
                ?>
                <optgroup label="<?php echo $prov['Nama_Provinsi'];?>">
                  <?php
                  $kabupaten=mysql_query("SELECT * from kabupaten WHERE Nama_Provinsi='$prov[Nama_Provinsi]' Order by Nama_Kabupaten");
                  while($kab=mysql_fetch_array($kabupaten))
                  {
                  ?>
                  <optgroup label="<?php echo $kab['Nama_Kabupaten'];?>">
                    <?php
                    $kecamatan=mysql_query("SELECT * from kecamatan WHERE Nama_Provinsi='$prov[Nama_Provinsi]' AND Nama_Kabupaten='$kab[Nama_Kabupaten]' Order by Nama_Kecamatan");
                    while($kec=mysql_fetch_array($kecamatan))
                      {
                      ?>
                      <option value="<?php echo $kec['Nama_Kecamatan'];?>"><?php echo $kec['Nama_Kecamatan'];?></option>
                      <?php
                      }
                      ?>
                  </optgroup>
                  <?php
                  }
                  ?>
                </optgroup>
                <?php
              }
              ?>
          </select>
        </div>
      </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Kelurahan Desa</label>
              </div>
              <div class="input-field col s12 m8">
                <select name ='kelurahan_desa'>
                  <option>Pilih Kelurahan Desa</option>
                  <?php
                  $provinsi=mysql_query("SELECT * from provinsi order by Nama_Provinsi");
                  while($prov=mysql_fetch_array($provinsi))
                  {
                  ?>
                  <optgroup label="<?php echo $prov['Nama_Provinsi'];?>">
                    <?php
                    $kabupaten=mysql_query("SELECT * from kabupaten WHERE Nama_Provinsi='$prov[Nama_Provinsi]' Order by Nama_Kabupaten");
                    while($kab=mysql_fetch_array($kabupaten))
                    {
                    ?>
                    <optgroup label="<?php echo $kab['Nama_Kabupaten'];?>">
                      <?php
                      $kecamatan=mysql_query("SELECT * from kecamatan WHERE Nama_Provinsi='$prov[Nama_Provinsi]' AND Nama_Kabupaten='$kab[Nama_Kabupaten]' Order by Nama_Kecamatan");
                      while($kec=mysql_fetch_array($kecamatan))
                      {
                      ?>
                      <optgroup label="<?php echo $kec['Nama_Kecamatan'];?>">
                        <?php
                        $kelurahan_desa=mysql_query("SELECT * from kelurahan_desa WHERE Nama_Provinsi='$prov[Nama_Provinsi]' AND Nama_Kabupaten='$kab[Nama_Kabupaten]' AND Nama_Kecamatan='$kec[Nama_Kecamatan]' Order by Nama_Desa");
                        while($keldes=mysql_fetch_array($kelurahan_desa))
                        {
                        ?>
                        <option value="<?php echo $keldes['Nama_Desa'];?>"><?php echo $keldes['Nama_Desa'];?></option>
                        <?php
                        }
                        ?>
                      </optgroup>
                      <?php
                      }
                      ?>
                    </optgroup>
                    <?php
                    }
                    ?>
                  </optgroup>
                  <?php
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Foto Profil</label>
              </div>
              <div class="input-field file-field col s12 m8">
                <div class="btn">
                  <span>File</span>
                  <input type="file" name="foto">
                </div>
                <div class="file-path-wrapper">
                  <input class="file-path validate" type="text">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
              </div>
              <div class="input-field col s12 m8">
                <input type="submit" value="Daftar" onclick="$('#un').prop('disabled', false);" class="btn-large right">
              </div>
            </div>


            </form><br><br>
          </div>
        </div>

        <br>
      </div>

    </div>
    </div>
    </div>

  <br><br>

  <footer class="page-footer teal darken-4">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Apa itu Dodolantani?</h5>
          <p class="grey-text text-lighten-4">Kami adalah sebuah layanan online yang melayani penjual-belian barang dan bahan yang berhubungan dengan pertanian. Anda dapat melakukan pembelian maupun penjualan. Website ini dibuat dengan tujuan untuk memenuhi syarat tugas akhir.</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">A</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">B</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      By <a class="orange-text text-lighten-3" href="#">72130021</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="js/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('select').material_select();
      $('.tooltipped').tooltip({delay: 50});
      $('.datepicker').pickadate({
        format: 'yyyy/mm/dd',
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false // Close upon selecting a date,
      });
    });
    </script>
  </body>
</html>
