<?php
session_start();
if(isset($_SESSION['username'])){
  header("Location:index.php");
}else{
  include "headers/headerguest.php";
}


mysql_connect("localhost", "root", "");
mysql_select_db("iais_ukdw");


?>


  <br>
 <div class="container">
    <div class="section"> 

      <!--   Side bar   -->
      <div class="row">
        <div class="col s12 m7 offset-m2" style="background-color:#f5f5f5;">
          <br>
          <div class="icon-block">
            <h4>Daftar Dodolantani</h4>
            <?php
            if(isset($_GET['msg'])){
            ?>
            <h6 class="red-text"><?php echo $_GET['msg']; ?> </h6>
            <?php
            }else
            echo "<br>";
          ?>
            <br>
            <form action="func/insertRegistrasi.php" method="post">
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Username</label>
              </div>
              <div class="input-field col s12 m8">
                <input type="text" id="username" name="username" required>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Password</label>
              </div>
              <div class="input-field col s12 m8">
                <input type="Password" name="password" id="password" required>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Pin</label>
              </div>
              <div class="input-field col s12 m8">
                <input type="text" name="pin" id="pin" maxlength="6" placeholder="Masukkan pin berupa angka." onkeypress='return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46' required>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
              </div>
              <div class="input-field col s12 m8">
                <input type="submit" value="Lanjut" class="btn-large right">
              </div>
            </div>
            </form><br><br>
          </div>
        </div>

        <br>
      </div>

    </div>
    </div>
    </div>

  <br><br>

  <footer class="page-footer teal darken-4">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Apa itu Dodolantani?</h5>
          <p class="grey-text text-lighten-4">Kami adalah sebuah layanan online yang melayani penjual-belian barang dan bahan yang berhubungan dengan pertanian. Anda dapat melakukan pembelian maupun penjualan. Website ini dibuat dengan tujuan untuk memenuhi syarat tugas akhir.</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">A</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">B</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      By <a class="orange-text text-lighten-3" href="#">72130021</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="js/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('select').material_select();
      $('.tooltipped').tooltip({delay: 50});
      $('.datepicker').pickadate({
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false // Close upon selecting a date,
      });
    });
    </script>
  </body>
</html>
