<?php
ob_start();
mysql_connect("localhost", "root", "");
mysql_select_db("iais_ukdw");
//============================================================+
// File name   : example_001.php
// Begin       : 2008-03-04
// Last Update : 2013-05-14
//
// Description : Example 001 for TCPDF class
//               Default Header and Footer
//
// Author: Nicola Asuni
//
// (c) Copyright:
//               Nicola Asuni
//               Tecnick.com LTD
//               www.tecnick.com
//               info@tecnick.com
//============================================================+

/**
 * Creates an example PDF TEST document using TCPDF
 * @package com.tecnick.tcpdf
 * @abstract TCPDF - Example: Default Header and Footer
 * @author Nicola Asuni
 * @since 2008-03-04
 */

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, 'A5', true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('GressSI');
$pdf->SetTitle('Nota Transaksi');

// set default header data
//$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE, PDF_HEADER_STRING, array(0,64,55), array(0,64,18));
//$pdf->setFooterData(array(0,64,0), array(0,64,128));

// set header and footer fonts
$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
$pdf->SetMargins('10', PDF_MARGIN_TOP, '10');
$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
  require_once(dirname(__FILE__).'/lang/eng.php');
  $pdf->setLanguageArray($l);
}

// datadataa
//include '../../func/func-nota.php';

// ---------------------------------------------------------

// set default font subsetting mode
$pdf->setFontSubsetting(true);

// Set font
// dejavusans is a UTF-8 Unicode font, if you only need to
// print standard ASCII chars, you can use core fonts like
// helvetica or times to reduce file size.
$pdf->SetFont('times', '', 8.5, '', true);

// Add a page
// This method has several options, check the source code documentation for more information.
$pdf->AddPage();

// set text shadow effect
$pdf->setTextShadow(array('enabled'=>true, 'depth_w'=>0.2, 'depth_h'=>0.2, 'color'=>array(196,196,196), 'opacity'=>1, 'blend_mode'=>'Normal'));

// Set some content to print
$html = '
<h3>Laporan Pembelian</h3><br>
<table>
    <thead>
      <tr>
        <th><center>NO</center></th>
        <th style="width:60px"><center>TANGGAL</center></th>
        <th style="width:20px"><center>ID</center></th>
        <th><center>NAMA USER</center></th>
        <th style="width:70px"><center>BARANG</center></th>
        <th><center>QTY</center></th>
        <th><center>HARGA</center></th>
        <th><center>JUMLAH</center></th>
        <th><center>STATUS</center></th>
      </tr>
    </thead>
    <tbody>';
    $total=0;
    $totalqty=0;
    if(($_GET['nama']!='') AND ($_GET['tanggal1']=='') AND ($_GET['tanggal2']==''))
    {
      $hasil=mysql_query("SELECT * FROM trans_permintaan JOIN trans_pembayaran ON trans_pembayaran.ID_Permintaan=trans_permintaan.ID_Permintaan WHERE Status='Sudah Dibayar' AND Status_Transaksi='Valid' AND trans_permintaan.ID_User='".$_GET['nama']."'");
      $no=1;
      while($baris=mysql_fetch_array($hasil)){
        $get_data = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE ID_Penawaran='".$baris['ID_Penawaran']."'");
        $barang = mysql_fetch_array($get_data);
        $get_data1 = mysql_query("SELECT * FROM trans_pembayaran WHERE ID_Permintaan='".$baris['ID_Permintaan']."'");
        $barang1 = mysql_fetch_array($get_data1);
        $cekkat = substr($barang['ID'],0,3);
          if($cekkat=="BAH")
          {
            $ambilnamat = mysql_query("SELECT * FROM master_bahan_tani WHERE ID_Bahan='{$barang['ID']}'");
            $ambilnama = mysql_fetch_array($ambilnamat);
            $nama=$ambilnama['Nama_Bahan'];
            $path="bahan";
          }
          elseif($cekkat=="HAS")
          {
            $ambilnamat = mysql_query("SELECT * FROM master_hasil_tani WHERE ID_Hasil='{$barang['ID']}'");
            $ambilnama = mysql_fetch_array($ambilnamat);
            $nama=$ambilnama['Nama_Hasil'];
            $path="produk";
          }
          elseif($cekkat=="ALA")
          {
            $ambilnamat = mysql_query("SELECT * FROM master_alat_tani WHERE ID_Alat='{$barang['ID']}'");
            $ambilnama = mysql_fetch_array($ambilnamat);
            $nama=$ambilnama['Nama_Alat'];
            $path="alat";
          };
          $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$baris['ID_User']}'");
          $ambiluser = mysql_fetch_array($ambilusert);
          $namauser=$ambiluser['nama'];
      $html.='
      <tr>
        <td><center>'.$no++.'</center></td>
        <td style="width:60px"><center>'.$baris['Tgl_Permintaan'].'</center></td>
        <td style="width:20px"><center>'.$baris['ID_Permintaan'].'</center></td>
        <td><center>'.$ambiluser['nama'].'</center></td>
        <td style="width:80px"><center>'.$nama.'</center></td>
        <td><center>'.$baris['Qty'].'</center></td>
        <td><center>'.$barang['Harga'].'</center></td>
        <td><center>'.$barang1['Jumlah_Pembayaran'].'</center></td>
        <td><center>'.$baris['Status'].'</center></td>
      </tr>';
      $total+=$barang1['Jumlah_Pembayaran'];
      $totalqty+=$baris['Qty'];
      }
    }
  elseif(($_GET['nama']=='') AND ($_GET['tanggal1']!='') AND ($_GET['tanggal2']!=''))
    {
      $hasil=mysql_query("SELECT * FROM trans_permintaan JOIN trans_pembayaran ON trans_pembayaran.ID_Permintaan=trans_permintaan.ID_Permintaan WHERE Status='Sudah Dibayar' AND Status_Transaksi='Valid' AND Tgl_Permintaan>='".$_GET['tanggal1']."' AND Tgl_Permintaan<='".$_GET['tanggal2']."'");
      $no=1;
      while($baris=mysql_fetch_array($hasil)){
        $get_data = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE ID_Penawaran='".$baris['ID_Penawaran']."'");
        $barang = mysql_fetch_array($get_data);
        $get_data1 = mysql_query("SELECT * FROM trans_pembayaran WHERE ID_Permintaan='".$baris['ID_Permintaan']."'");
        $barang1 = mysql_fetch_array($get_data1);
        $cekkat = substr($barang['ID'],0,3);
          if($cekkat=="BAH")
          {
            $ambilnamat = mysql_query("SELECT * FROM master_bahan_tani WHERE ID_Bahan='{$barang['ID']}'");
            $ambilnama = mysql_fetch_array($ambilnamat);
            $nama=$ambilnama['Nama_Bahan'];
            $path="bahan";
          }
          elseif($cekkat=="HAS")
          {
            $ambilnamat = mysql_query("SELECT * FROM master_hasil_tani WHERE ID_Hasil='{$barang['ID']}'");
            $ambilnama = mysql_fetch_array($ambilnamat);
            $nama=$ambilnama['Nama_Hasil'];
            $path="produk";
          }
          elseif($cekkat=="ALA")
          {
            $ambilnamat = mysql_query("SELECT * FROM master_alat_tani WHERE ID_Alat='{$barang['ID']}'");
            $ambilnama = mysql_fetch_array($ambilnamat);
            $nama=$ambilnama['Nama_Alat'];
            $path="alat";
          };
          $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$baris['ID_User']}'");
          $ambiluser = mysql_fetch_array($ambilusert);
          $namauser=$ambiluser['nama'];
      $html.='
      <tr>
        <td><center>'.$no++.'</center></td>
        <td style="width:60px"><center>'.$baris['Tgl_Permintaan'].'</center></td>
        <td style="width:20px"><center>'.$baris['ID_Permintaan'].'</center></td>
        <td><center>'.$ambiluser['nama'].'</center></td>
        <tdstyle="width:80px><center>'.$nama.'</center></td>
        <td><center>'.$baris['Qty'].'</center></td>
        <td><center>'.$barang['Harga'].'</center></td>
        <td><center>'.$barang1['Jumlah_Pembayaran'].'</center></td>
        <td><center>'.$baris['Status'].'</center></td>
      </tr>';
      $total+=$barang1['Jumlah_Pembayaran'];
      $totalqty+=$baris['Qty'];
      }
    }
  elseif(($_GET['nama']!='') AND ($_GET['tanggal1']!='') AND ($_GET['tanggal2']!=''))
    {
      $hasil=mysql_query("SELECT * FROM trans_permintaan JOIN trans_pembayaran ON trans_pembayaran.ID_Permintaan=trans_permintaan.ID_Permintaan WHERE Status='Sudah Dibayar' AND Status_Transaksi='Valid' AND Tgl_Permintaan>='".$_GET['tanggal1']."' AND Tgl_Permintaan<='".$_GET['tanggal2']."' AND trans_permintaan.ID_User='".$_GET['nama']."'");
      $no=1;
      while($baris=mysql_fetch_array($hasil)){
        $get_data = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE ID_Penawaran='".$baris['ID_Penawaran']."'");
        $barang = mysql_fetch_array($get_data);
        $get_data1 = mysql_query("SELECT * FROM trans_pembayaran WHERE ID_Permintaan='".$baris['ID_Permintaan']."'");
        $barang1 = mysql_fetch_array($get_data1);
        $cekkat = substr($barang['ID'],0,3);
          if($cekkat=="BAH")
          {
            $ambilnamat = mysql_query("SELECT * FROM master_bahan_tani WHERE ID_Bahan='{$barang['ID']}'");
            $ambilnama = mysql_fetch_array($ambilnamat);
            $nama=$ambilnama['Nama_Bahan'];
            $path="bahan";
          }
          elseif($cekkat=="HAS")
          {
            $ambilnamat = mysql_query("SELECT * FROM master_hasil_tani WHERE ID_Hasil='{$barang['ID']}'");
            $ambilnama = mysql_fetch_array($ambilnamat);
            $nama=$ambilnama['Nama_Hasil'];
            $path="produk";
          }
          elseif($cekkat=="ALA")
          {
            $ambilnamat = mysql_query("SELECT * FROM master_alat_tani WHERE ID_Alat='{$barang['ID']}'");
            $ambilnama = mysql_fetch_array($ambilnamat);
            $nama=$ambilnama['Nama_Alat'];
            $path="alat";
          };
          $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$baris['ID_User']}'");
          $ambiluser = mysql_fetch_array($ambilusert);
          $namauser=$ambiluser['nama'];
      $html.='
      <tr>
        <td><center>'.$no++.'</center></td>
        <td style="width:60px"><center>'.$baris['Tgl_Permintaan'].'</center></td>
        <td style="width:20px"><center>'.$baris['ID_Permintaan'].'</center></td>
        <td><center>'.$ambiluser['nama'].'</center></td>
        <td style="width:80px"><center>'.$nama.'</center></td>
        <td><center>'.$baris['Qty'].'</center></td>
        <td><center>'.$barang['Harga'].'</center></td>
        <td><center>'.$barang1['Jumlah_Pembayaran'].'</center></td>
        <td><center>'.$baris['Status'].'</center></td>
      </tr>';
      $total+=$barang1['Jumlah_Pembayaran'];
      $totalqty+=$baris['Qty'];
      }
    }
  else
  {
    $hasil=mysql_query("SELECT * FROM trans_permintaan JOIN trans_pembayaran ON trans_pembayaran.ID_Permintaan=trans_permintaan.ID_Permintaan WHERE Status='Sudah Dibayar' AND Status_Transaksi='Valid'");
    $no=1;
    while($baris=mysql_fetch_array($hasil)){
      $get_data = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE ID_Penawaran='".$baris['ID_Penawaran']."'");
      $barang = mysql_fetch_array($get_data);
      $get_data1 = mysql_query("SELECT * FROM trans_pembayaran WHERE ID_Permintaan='".$baris['ID_Permintaan']."'");
      $barang1 = mysql_fetch_array($get_data1);
      $cekkat = substr($barang['ID'],0,3);
        if($cekkat=="BAH")
        {
          $ambilnamat = mysql_query("SELECT * FROM master_bahan_tani WHERE ID_Bahan='{$barang['ID']}'");
          $ambilnama = mysql_fetch_array($ambilnamat);
          $nama=$ambilnama['Nama_Bahan'];
          $path="bahan";
        }
        elseif($cekkat=="HAS")
        {
          $ambilnamat = mysql_query("SELECT * FROM master_hasil_tani WHERE ID_Hasil='{$barang['ID']}'");
          $ambilnama = mysql_fetch_array($ambilnamat);
          $nama=$ambilnama['Nama_Hasil'];
          $path="produk";
        }
        elseif($cekkat=="ALA")
        {
          $ambilnamat = mysql_query("SELECT * FROM master_alat_tani WHERE ID_Alat='{$barang['ID']}'");
          $ambilnama = mysql_fetch_array($ambilnamat);
          $nama=$ambilnama['Nama_Alat'];
          $path="alat";
        };
        $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$baris['ID_User']}'");
        $ambiluser = mysql_fetch_array($ambilusert);
        $namauser=$ambiluser['nama'];
      $html.='
      <tr>
        <td><center>'.$no++.'</center></td>
        <td style="width:60px"><center>'.$baris['Tgl_Permintaan'].'</center></td>
        <td style="width:20px"><center>'.$baris['ID_Permintaan'].'</center></td>
        <td><center>'.$ambiluser['nama'].'</center></td>
        <td style="width:80px"><center>'.$nama.'</center></td>
        <td><center>'.$baris['Qty'].'</center></td>
        <td><center>'.$barang['Harga'].'</center></td>
        <td><center>'.$barang1['Jumlah_Pembayaran'].'</center></td>
        <td><center>'.$baris['Status'].'</center></td>
      </tr>';
        $total+=$barang1['Jumlah_Pembayaran'];
        $totalqty+=$baris['Qty'];
      }
  }
$html.='
<tr>
    <td><center>TOTAL</center></td>
    <td style="width:60px"><center></center></td>
    <td style="width:20px"><center></center></td>
    <td><center></center></td>
    <td><center></center></td>
    <td><center>'.$totalqty.'</center></td>
    <td><center></center></td>
    <td><center>Rp '.number_format($total).'</center></td>
    <td><center></center></td>
</tr>    
    </tbody>
</table><br>';


// Print text using writeHTMLCell()
$pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);

// ---------------------------------------------------------
ob_end_clean();
// Close and output PDF document
// This method has several options, check the source code documentation for more information.
$pdf->Output('nota.pdf', 'I');

//============================================================+
// END OF FILE
//============================================================+
