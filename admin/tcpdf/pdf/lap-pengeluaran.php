<?php

// Include the main TCPDF library (search for installation path).
require_once('tcpdf_include.php');

// create new PDF document
$pdf = new TCPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);

// set document information
$pdf->SetCreator(PDF_CREATOR);
$pdf->SetAuthor('SIGress');
$pdf->SetTitle('Laporan Keuangan Bulanan');

// remove default header/footer
$pdf->setPrintHeader(false);
$pdf->setPrintFooter(false);

// set default monospaced font
$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

// set margins
// $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);

// set auto page breaks
$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
if (@file_exists(dirname(__FILE__).'/lang/eng.php')) {
	require_once(dirname(__FILE__).'/lang/eng.php');
	$pdf->setLanguageArray($l);
}

// ---------------------------------------------------------

// set font
$pdf->SetFont('times', 12);

// add a page
$pdf->AddPage();

// data utk laporan
mysql_connect("localhost", "root", "");
mysql_select_db("gressdb");

include '../../func/func-lapkeluar.php';
$pengeluaran_an = $pengeluaran - $setup_total;
$sabun_an = $rec_sabun - $setup_sabun;
$pewangi_an = $rec_pewangi - $setup_pewangi;
$plastik_an = $rec_plastik - $setup_plastik;


// create some HTML content
$html = '
<style>
table, th, td {
    border: 1px solid black;
    border-collapse: collapse;
    padding:15px;
}
th, td {
    padding: 5px;
    text-align: left;    
}
.alignleft {
	float: left;
}
.alignright {
	float: right;
}
</style>
<div align="center"><h1>Laporan Pengeluaran Periode '.$bulan.' '.$tahun.'</h1></div>
<br><br><br>
<table border="1">
<tr>
<td>
<b>Pengeluaran Tercatat :</b> <br><br>
Total : Rp '.number_format($pengeluaran).'<br><br>
Pengeluaran dari Sabun : Rp '.number_format($rec_sabun).'<br>
Pengeluaran dari Pewangi : Rp '.number_format($rec_pewangi).'<br>
Pengeluaran dari Plastik : Rp '.number_format($rec_plastik).'
</td>
<td>
<b>Pengeluaran Bulanan Standar :</b> <br><br>
Total : Rp '.number_format($setup_total).'<br><br>

Pengeluaran dari Sabun : Rp '.number_format($setup_sabun).'<br>
Pengeluaran dari Pewangi : Rp '.number_format($setup_pewangi).'<br>
Pengeluaran dari Plastik : Rp '.number_format($setup_plastik).'
</td>
</tr>
<tr>
<td colspan="2">
<b>Analisis Perbandingan (Pengeluaran - Standar): </b><br><br>
Total : Rp '.number_format($pengeluaran_an).'<br><br>

Pengeluaran dari Sabun : Rp '.number_format($sabun_an).'<br>
Pengeluaran dari Pewangi : Rp '.number_format($pewangi_an).'<br>
Pengeluaran dari Plastik : Rp '.number_format($plastik_an).'
</td>
</tr></table>
<br>
<b><h3>Daftar Pengeluaran Harian</h3></b><br>
<table>
<tr>
    <th><b>#</b></th>
    <th><b>Tanggal</b></th>
    <th><b>Sabun</b></th>
    <th><b>Pewangi</b></th>
    <th><b>Plastik</b></th>
</tr>';
$hsl = mysql_query("SELECT * from usage_rec where MONTH(tanggal)='".$bulan_int."' and YEAR(tanggal)='".$tahun."'");
    $no=1;
    if(mysql_num_rows($hsl)==0){
        $html .=
        "<tr><td>-</td>
         <td>-</td>
         <td>-</td>
         <td>-</td>
         <td>-</td>
          </tr>";
     }else{
     while($brs = mysql_fetch_array($hsl)){
        $html .= '<tr>
        <td>'.$no++.'</td>
        <td>'.$brs['tanggal'].'</td>
        <td>Rp '.number_format($brs['sabun'] * $harga_sabun).'</td>
        <td>Rp '.number_format($brs['pewangi'] * $harga_pewangi).'</td>
        <td>Rp '.number_format($brs['plastik'] * $harga_plastik).'</td></tr>'; }}
    $html .='</table>';


// output the HTML content
$pdf->writeHTML($html, true, false, true, false, '');


// ---------------------------------------------------------

//Close and output PDF document
$pdf->Output('Laporan Keuangan '.$bulan.' '.$tahun.'.pdf', 'D');

//============================================================+
// END OF FILE
//============================================================+
