<?php
mysql_connect("localhost", "root", "");
mysql_select_db("iais_ukdw");
include "../headers/headeradmin.php";
session_start();
if(isset($_SESSION['username'])){
  $un = $_SESSION['username'];
  $tipe = $_SESSION['tipe'];
  if($tipe!=1)
  {
    header("Location:../index.php");
  }
}else{
  header("Location:../index.php");
}
//$id = $_GET['ID_User'];

?>
  <style type="text/css">
  /* just for jsfiddle, next/prev arrows on carousel */
  .middle-indicator{
   position:absolute;
   top:50%;
   }
  .middle-indicator-text{
   font-size: 4.2rem;
  }
  a.middle-indicator-text{
    color:white !important;
  }
  .content-indicator{
    width: 64px;
    height: 64px;
    background: none;
    -moz-border-radius: 50px;
    -webkit-border-radius: 50px;
    border-radius: 50px; 
  }
    .indicators{
    visibility: hidden;
  }
  img.items{
    position: relative;
    float: left;
    width: 100%;
    height: 200px;
    padding-top: 10px;
    padding-bottom: 10px;
    background-position: 50% 50%;
    background-repeat: no-repeat;
    background-size: cover;
  }
  a.cat{
    color: #999;
  }
  a.cat:hover{
    border-bottom: 2px solid black;
    color: #000 ;
  }
  a.catactive{
    border-bottom: 0;
    color: #000;
  }
  a.itemname{
    border-bottom: 2px solid black;
    color: #000;
  }
  a.itemname:hover{
    border-bottom: 0;
    color: #999;
  }
  </style>
  
  <div class="container">
    <div class="section">
      <h3>Kirim Notifikasi Ke Pengguna</h3>
          <form method="post" action="addnotifikasi.php">
          	<table class="table table-bordered table-striped" id="thetable">
            <div class="row">
              <div class="input-field col s6 m3">
                <label class="black-text">ID Permintaan</label>
              </div>
              <div class="input-field col s6 m3">
                <input type="text" id="idpermintaan" name="idpermintaan" value="<?php echo $_GET['idpermintaan']; ?>">
              </div>
            </div>
            <div class="row">
              <div class="input-field col s6 m3">
                <label class="black-text">ID User</label>
              </div>
              <div class="input-field col s6 m3">
                <input type="text" name="nama" value="<?php echo $_GET['nama']; ?>" required>
              </div>
            </div>
             <div class="row">
              <div class="input-field col s6 m3">
                <label class="black-text">Isi Pesan</label>
              </div>
              <div class="input-field col s6 m3">
                <input type="text" name="pesan" id="pesan" required>
              </div>
            </div>
            </div><br><br>
            <input type="submit" value="Kirim" class="waves-effect waves-light btn modal-trigger"/>
          </form>	
</table>
      </div>
        
  </div>


</div>
</div>
                
                     
        

 


  <footer class="page-footer teal darken-4">
    <br>
    <br>
    <div class="container">
    </div>  
    <div class="footer-copyright"> 
      <div class="container">
      <br> 
      By <a class="orange-text text-lighten-3" href="#">72130021</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.js"></script>
  <script src="../js/init.js"></script>
  <script type="text/javascript">
  // carousel initializing & autoplay
    $(document).ready(function(){
      $('.carousel').carousel();
      autoplay()   
      function autoplay() {
          $('.carousel').carousel('next');
          setTimeout(autoplay, 4500);
      }
    });
   // start carrousel
   $('.carousel.carousel-slider').carousel({
      fullWidth: true,
      indicators: false
   });


   // move next carousel
   $('.moveNextCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.carousel').carousel('next');
   });

   // move prev carousel
   $('.movePrevCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.carousel').carousel('prev');
   });
   $(document).ready(function() {
      $(".button-collapse").sideNav();
      $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: false, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation

    });
    });
  </script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('select').material_select();
      $('.tooltipped').tooltip({delay: 50});
      $('.modal').modal();
    });
    </script>

  </body>
</html>
