<?php
session_start();

if(isset($_SESSION['username'])){
  $un = $_SESSION['username'];
  $tipe = $_SESSION['tipe'];
  if($tipe!=2)
  {
    header("Location:admin/home.php");
  }
  include "headers/headerlogged2.php";
}else{
  include "headers/headerguest.php";
}



mysql_connect("localhost", "root", "");
mysql_select_db("iais_ukdw");

if(isset($_GET['kate'])){
  $kate = $_GET['kate'];
}else{
  $kate = 'semua';
}

?>
  <style type="text/css">
  /* just for jsfiddle, next/prev arrows on carousel */
  .middle-indicator{
   position:absolute;
   top:50%;
   }
  .middle-indicator-text{
   font-size: 4.2rem;
  }
  a.middle-indicator-text{
    color:white !important;
  }
  .content-indicator{
    width: 64px;
    height: 64px;
    background: none;
    -moz-border-radius: 50px;
    -webkit-border-radius: 50px;
    border-radius: 50px; 
  }
    .indicators{
    visibility: hidden;
  }
  img.items{
    position: relative;
    float: left;
    width: 100%;
    height: 200px;
    padding-top: 10px;
    padding-bottom: 10px;
    background-position: 50% 50%;
    background-repeat: no-repeat;
    background-size: cover;
  }
  a.cat{
    color: #999;
  }
  a.cat:hover{
    border-bottom: 2px solid black;
    color: #000 ;
  }
  a.catactive{
    border-bottom: 0;
    color: #000;
  }
  a.itemname{
    border-bottom: 2px solid black;
    color: #000;
  }
  a.itemname:hover{
    border-bottom: 0;
    color: #999;
  }
  </style>
  <div class="section no-pad-bot" id="index-banner">
    <div class="container">
  <div class="carousel carousel-slider center" data-indicators="true">
    <div class="carousel-fixed-item center middle-indicator">
     <div class="left">
      <a href="Previo" class="movePrevCarousel middle-indicator-text waves-effect waves-light content-indicator"><i class="material-icons left  middle-indicator-text">chevron_left</i></a>
     </div>
     
     <div class="right">
     <a href="Siguiente" class="moveNextCarousel middle-indicator-text waves-effect waves-light content-indicator"><i class="material-icons right middle-indicator-text">chevron_right</i></a>
     </div>
    </div>
    <div class="carousel-item" href="#one!">
      <img src="pictures/hoe.jpg">
    </div>
    <div class="carousel-item amber white-text" href="#two!">
      <img src="pictures/gardentools.jpg">
    </div>
    <div class="carousel-item green white-text" href="#three!">
      <img src="pictures/axe.jpg">
    </div>
  </div>


    </div>
  </div>

  <br><br>
  <div class="container">
    <div class="section">

      <!--   Icon Section   -->
      <div class="row">
        <?php
          $nama="";
          $namauser="";
          $kabupaten="";
          $path="";

          if($kate=="semua"){
            $hasil = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE Status_Produk='Aktif'");
            while ($brs = mysql_fetch_array($hasil)){
              $cekkat = substr($brs['ID'],0,3);
              if($cekkat=="BAH")
              {
                $iditem = $brs['ID'];
                $ambilnamat = mysql_query("SELECT * FROM master_bahan_tani WHERE ID_Bahan='{$brs['ID']}'");
                $ambilnama = mysql_fetch_array($ambilnamat);
                $nama=$ambilnama['Nama_Bahan'];
                $path="bahan";
              }
              elseif($cekkat=="HAS")
              {
                $ambilnamat = mysql_query("SELECT * FROM master_hasil_tani WHERE ID_Hasil='{$brs['ID']}'");
                $ambilnama = mysql_fetch_array($ambilnamat);
                $nama=$ambilnama['Nama_Hasil'];
                $path="produk";
              }
              elseif($cekkat=="ALA")
              {
                $ambilnamat = mysql_query("SELECT * FROM master_alat_tani WHERE ID_Alat='{$brs['ID']}'");
                $ambilnama = mysql_fetch_array($ambilnamat);
                $nama=$ambilnama['Nama_Alat'];
                $path="alat";
              };
              $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$brs['ID_User']}'");
              $ambiluser = mysql_fetch_array($ambilusert);
              $namauser=$ambiluser['nama'];
              $kabupaten=$ambiluser['kabupaten'];
          ?> 
          <div class="col s12 m4">
            <div class="icon-block" style="border:1px solid #009688"> 
              <img src="pictures/items/<?php echo $path?>/<?php echo $brs['Gambar1']?>" class="items center" >
              <h6><a class="itemname" href="pengguna/detailitem.php?id=<?php echo $brs['ID']; ?>&kode=<?php echo $brs['ID_Penawaran']; ?>&user=<?php echo $un; ?>"><?php echo $nama?></a></h6>
              <p class="light">
                <i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star_half</i><br> 
                <b><?php echo 'Rp '.number_format($brs['Harga']); ?></b><br><br>
                <i class="gray-text material-icons prefix">person</i> <?php echo $namauser?>, <?php echo $kabupaten?>
              </p>
              </div>
              <br>
          </div>
        <?php }}elseif($kate=="alat"){
          $hasil = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE Status='Aktif'");
            while ($brs = mysql_fetch_array($hasil)){
              $cekkat = substr($brs['ID'],0,3);
              if($cekkat=="ALA")
              {
                $ambilnamat = mysql_query("SELECT * FROM master_alat_tani WHERE ID_Alat='{$brs['ID']}'");
                $ambilnama = mysql_fetch_array($ambilnamat);
                $nama=$ambilnama['Nama_Alat'];
                $path="alat";
                $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$brs['ID_User']}'");
                $ambiluser = mysql_fetch_array($ambilusert);
                $namauser=$ambiluser['nama'];
                $kabupaten=$ambiluser['kabupaten']; ?>

          <div class="col s12 m4">
            <div class="icon-block" style="border:1px solid #009688"> 
              <img src="pictures/items/<?php echo $path?>/<?php echo $brs['Gambar1']?>" class="items center" >
              <h6><a class="itemname" href="pengguna/detailitem.php?id=<?php echo $brs['ID']; ?>&kode=<?php echo $brs['ID_Penawaran']; ?>"><?php echo $nama?></a></h6>
              <p class="light">
                <i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star_half</i><br> 
                <b><?php echo 'Rp '.number_format($brs['Harga']); ?></b><br><br>
                <i class="gray-text material-icons prefix">person</i> <?php echo $namauser?>, <?php echo $kabupaten?>
              </p>
              </div>
              <br>
          </div>
        <?php }}}elseif($kate=="bahan")
              {
            $hasil = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE Status='Aktif'");
              while ($brs = mysql_fetch_array($hasil)){
                $cekkat = substr($brs['ID'],0,3);
                if($cekkat=="BAH")
                {
                $iditem = $brs['ID'];
                $ambilnamat = mysql_query("SELECT * FROM master_bahan_tani WHERE ID_Bahan='{$brs['ID']}'");
                $ambilnama = mysql_fetch_array($ambilnamat);
                $nama=$ambilnama['Nama_Bahan'];
                $path="bahan";
                $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$brs['ID_User']}'");
                $ambiluser = mysql_fetch_array($ambilusert);
                $namauser=$ambiluser['nama'];
                $kabupaten=$ambiluser['kabupaten'];?>
          <div class="col s12 m4">
            <div class="icon-block" style="border:1px solid #009688"> 
              <img src="pictures/items/<?php echo $path?>/<?php echo $brs['Gambar1']?>" class="items center" >
              <h6><a class="itemname" href="pengguna/detailitem.php?id=<?php echo $brs['ID']; ?>&kode=<?php echo $brs['ID_Penawaran']; ?>"><?php echo $nama?></a></h6>
              <p class="light">
                <i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star_half</i><br> 
                <b><?php echo 'Rp '.number_format($brs['Harga']); ?></b><br><br>
                <i class="gray-text material-icons prefix">person</i> <?php echo $namauser?>, <?php echo $kabupaten?>
              </p>
              </div>
              <br>
          </div>
        <?php }}}elseif($kate=="hasil")
              {
            $hasil = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE Status='Aktif'");
              while ($brs = mysql_fetch_array($hasil)){
                $cekkat = substr($brs['ID'],0,3);
                if($cekkat=="HAS")
                {
                $ambilnamat = mysql_query("SELECT * FROM master_hasil_tani WHERE ID_Hasil='{$brs['ID']}'");
                $ambilnama = mysql_fetch_array($ambilnamat);
                $nama=$ambilnama['Nama_Hasil'];
                $path="produk"; 
                $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$brs['ID_User']}'");
                $ambiluser = mysql_fetch_array($ambilusert);
                $namauser=$ambiluser['nama'];
                $kabupaten=$ambiluser['kabupaten']; ?>
          <div class="col s12 m4">
            <div class="icon-block" style="border:1px solid #009688"> 
              <img src="pictures/items/<?php echo $path?>/<?php echo $brs['Gambar1']?>" class="items center" >
              <h6><a class="itemname" href="pengguna/detailitem.php?id=<?php echo $brs['ID']; ?>&kode=<?php echo $brs['ID_Penawaran']; ?>"><?php echo $nama?></a></h6>
              <p class="light">
                <i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star</i><i class="yellow-text material-icons">star_half</i><br> 
                <b><?php echo 'Rp '.number_format($brs['Harga']); ?></b><br><br>
                <i class="gray-text material-icons prefix">person</i> <?php echo $namauser ?>, <?php echo $kabupaten; ?>
              </p>
              </div>
              <br>
          </div>
        <?php }}} ?>                           
        </div>
      </div>
    </div>
    <br><br>
  </div>

  <footer class="page-footer teal darken-4">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Apa itu Dodolantani?</h5>
          <p class="grey-text text-lighten-4">Kami adalah sebuah layanan online yang melayani penjual-belian barang dan bahan yang berhubungan dengan pertanian. Anda dapat melakukan pembelian maupun penjualan. Website ini dibuat dengan tujuan untuk memenuhi syarat tugas akhir.</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">A</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">B</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      By <a class="orange-text text-lighten-3" href="#">72130021</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="js/jquery-2.1.1.min.js"></script>
  <script src="js/materialize.js"></script>
  <script src="js/init.js"></script>
  <script type="text/javascript">
  // carousel initializing & autoplay
    $(document).ready(function(){
      $('.carousel').carousel();
      autoplay()   
      function autoplay() {
          $('.carousel').carousel('next');
          setTimeout(autoplay, 4500);
      }
    });
   // start carrousel
   $('.carousel.carousel-slider').carousel({
      fullWidth: true,
      indicators: false
   });


   // move next carousel
   $('.moveNextCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.carousel').carousel('next');
   });

   // move prev carousel
   $('.movePrevCarousel').click(function(e){
      e.preventDefault();
      e.stopPropagation();
      $('.carousel').carousel('prev');
   });
   $(document).ready(function() {
      $(".button-collapse").sideNav();
      $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: false, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation
    });
    });
  </script>
  </body>
</html>
