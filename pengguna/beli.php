<?php
session_start();
if(isset($_SESSION['username'])){
  $un = $_SESSION['username'];
  $tipe = $_SESSION['tipe'];
  include "../headers/headerlogged.php";
}else{
  include "../headers/headerguest2.php";
  header("Location:../index.php");
}
if(isset($_SESSION['username'])){
  $un = $_SESSION['username'];
  $tipe = $_SESSION['tipe'];
  if($tipe!=2)
  {
    header("Location:../admin/home.php");
  }
}else{
  header("Location:../index.php");
}

 
if(isset($_GET['id']) && isset($_GET['kode'])){
  $id = $_GET['id'];
  $kode = $_GET['kode'];
}else{
  header("Location:../index.php");
}

$hasil = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE ID='$id'");
$brs = mysql_fetch_array($hasil);

$cekkat = substr($brs['ID'],0,3);
if($cekkat=="BAH")
{
  $iditem = $brs['ID'];
  $ambilnamat = mysql_query("SELECT * FROM master_bahan_tani WHERE ID_Bahan='{$brs['ID']}'");
  $ambilnama = mysql_fetch_array($ambilnamat);
  $nama=$ambilnama['Nama_Bahan'];
  $path="bahan";
}
elseif($cekkat=="HAS")
{
  $ambilnamat = mysql_query("SELECT * FROM master_hasil_tani WHERE ID_Hasil='{$brs['ID']}'");
  $ambilnama = mysql_fetch_array($ambilnamat);
  $nama=$ambilnama['Nama_Hasil'];
  $path="produk";
}
elseif($cekkat=="ALA")
{
  $ambilnamat = mysql_query("SELECT * FROM master_alat_tani WHERE ID_Alat='{$brs['ID']}'");
  $ambilnama = mysql_fetch_array($ambilnamat);
  $nama=$ambilnama['Nama_Alat'];
  $path="alat";
};


?>

  <style type="text/css">  
  img.items{
    position: relative;
    float: left;
    width: 100%;
    height: 100px;
    padding-top: 10px;
    padding-bottom: 10px;
    background-position: 50% 50%;
    background-repeat: no-repeat;
    background-size: cover;
  }
  .nav-wrapper .input-field input[type=search] {
    height: 64px;
  }
  .nav-wrapper input[type="search"]:focus {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
  }
  .input-field label {
    max-height: 64px;
  }
  .nav-wrapper input[type="search"]:focus ~ .label-icon.active {
    position: fixed;
    left: 10px;
  }
  .nav-wrapper input[type="search"]:focus ~ .closed {
    position: fixed;
    right: 10px;
  }
  </style>
</head>

  <br>
 <div class="container">
    <div class="section"> 

      <!--   Side bar   -->
      <div class="row" style="background-color:#f5f5f5;">
        <div class="col s12">
          <br>
          <div class="icon-block">
            <h4>Beli Barang</h4>
            <br>
            <form action="func/beliitem.php?id=<?php echo $id; ?>&kode=<?php echo $kode; ?>&do=2" method="post">
            <div class="row">
              <div class="input-field col s12 m2">
                <img src="../pictures/items/<?php echo $path?>/<?php echo $brs['Gambar1']?>" class="items center" >
              </div>
              <div class="input-field col s12 m10">
                <br>
                <b>Barang : </b><?php echo $nama?><br>  
                <b>Harga Barang : </b><?php echo 'Rp '.number_format($brs['Harga']); ?><br>
                <b>Stok Barang : </b><?php echo ($brs['Stok']); ?><br> 
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m2">
                <label class="black-text">Jumlah Beli</label>
              </div>
              <div class="input-field col s12 m2">
                <input type="number" min="0" max="<?php echo $brs['Stok'];?>" required name="jumlah" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46'>
              </div>
              <div class="input-field col s12 m2 offset-m1">
                <label class="black-text">Tanggal Kebutuhan</label>
              </div>
              <div class="input-field col s12 m3">
                <input type="text" name="tglbutuh" class="datepicker" required>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m2">
                <label class="black-text">Alamat</label>
              </div>
              <div class="input-field col s12 m10">
                <select name="alamat">
                  <?php 
                  $hasil = mysql_query("SELECT * FROM detail_alamat WHERE ID_User='$un'");
                  while ($brs = mysql_fetch_array($hasil)){
                  ?> 
                  <option value="<?php echo $brs['kode_alamat']; ?>"><?php echo  $brs['nama_penerima'].', '.$brs['nama_alamat'] ?>. <?php echo $brs['alamat_penerima'].', '.$brs['kabupaten'].', '.$brs['provinsi'].', '.$brs['kodepos'].'.' ?></option>
                  <?php } ?>
                </select>
                <a href="tambahalamat.php?id=<?php echo $id; ?>&kode=<?php echo $kode; ?>">Pilih Alamat lain?</a>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m2">
                <label class="black-text">Kurir</label>
              </div>
              <div class="input-field col s12 m10">
                <?php
                $kab1=0;
                $kab2=0;
                $hsl= mysql_query("SELECT * FROM detail_alamat");
                 while ($baris = mysql_fetch_array($hsl)){
                  $kab1 = $baris['kabupaten'];
                 }?>  
                  <?php
                $hsl2= mysql_query("SELECT * FROM master_detail_user where ID_User='$un'");
                 while ($baris2 = mysql_fetch_array($hsl2)){
                  $kab2 = $baris2['kabupaten'];  
                  }?> 
                <select name="kurir">
                <?php 
                    $hasil = mysql_query("SELECT * FROM kurir where kota_awal='$kab2' AND kota_tujuan='$kab1'");
                    while ($brs = mysql_fetch_array($hasil)){
                ?> 
                  <option value="<?php echo $brs['kode_kurir'] ?>"><?php echo $brs['nama_kurir']; ?></option>
                  <?php }?>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m2">
              </div>
              <div class="input-field col s12 m10">
                <input type="submit" value="Tambah ke Cart" class="btn right">
              </div>
            </div>


            </form><br><br>
          </div>
          <br><br>
        </div>
        <br>
      </div>

    </div>
    </div>
    </div>

  <br><br>

  <footer class="page-footer teal darken-4">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Apa itu Dodolantani?</h5>
          <p class="grey-text text-lighten-4">Kami adalah sebuah layanan online yang melayani penjual-belian barang dan bahan yang berhubungan dengan pertanian. Anda dapat melakukan pembelian maupun penjualan. Website ini dibuat dengan tujuan untuk memenuhi syarat tugas akhir.</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">A</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">B</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      By <a class="orange-text text-lighten-3" href="#">72130021</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.js"></script>
  <script src="../js/init.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('select').material_select();
      $('.tooltipped').tooltip({delay: 50});
      $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: false, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation
      });
      $('.datepicker').pickadate({
        format: 'yyyy/mm/dd',
        selectMonths: true, // Creates a dropdown to control month
        selectYears: 15, // Creates a dropdown of 15 years to control year,
        today: 'Today',
        clear: 'Clear',
        close: 'Ok',
        closeOnSelect: false // Close upon selecting a date,
      });
    });
    </script>
  </body>
</html>
