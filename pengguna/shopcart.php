<?php
session_start();
if(isset($_SESSION['username'])){
  $un = $_SESSION['username'];
  $tipe = $_SESSION['tipe'];
  include "../headers/headerlogged.php";
}else{
  include "../headers/headerguest2.php";
}
if(isset($_SESSION['username'])){
  $un = $_SESSION['username'];
  $tipe = $_SESSION['tipe'];
  if($tipe!=2)
  {
    header("Location:../admin/home.php");
  }
}else{
  header("Location:../index.php");
}
$tampung = 0;
?>
  <style type="text/css">
  img.items{
    position: relative;
    width: 80%;
    height: 150px;
    padding-top: 10px;
    padding-bottom: 10px;
    background-position: 50% 50%;
    background-repeat: no-repeat;
    background-size: cover;
  }
  a.cat{
    color: #999;
  }
  a.cat:hover{
    border-bottom: 2px solid black;
    color: #000 ;
  }
  a.catactive{
    border-bottom: 0;
    color: #000;
  }
  a.itemname{
    font-size: 18px;
    border-bottom: 1px solid black;
    color: #000;
  }
  a.itemname:hover{
    border-bottom: 0;
    color: #999;
  }
  </style>
  <br>

<?php if(isset($_GET['msg'])){ ?>
  <div class="container">
    <div class="section">
      <div class="row" style="background-color:#c8e6c9; padding:5px; color:#1b5e20">
        <div class="icon-block">
          Item telah ditambahkan.
        </div>
      </div>
    </div>
  </div>
<?php } ?>

  

  <div class="container">
    <div class="section">

      <div class="row">
        <div style="border-bottom:3px solid #009688">
          <h5 class="">Barang pada Keranjang Belanja Anda</h5> 
        </div>
      </div>
      <!--   Icon Section   -->
      <form>
      <?php 
          $items = mysql_query("select * from trans_permintaan where Status='Belum Dibayar' AND ID_User='".$un."'");
          $titip=0;
          while ($item = mysql_fetch_array($items)){
            $get_data = mysql_query("SELECT * FROM trans_penawaran_prod_tani JOIN trans_harga_prod ON trans_penawaran_prod_tani.ID_Produk=trans_harga_prod.ID_Produk JOIN master_produk_tani_commerce ON trans_harga_prod.ID_Produk=master_produk_tani_commerce.ID_Produk WHERE ID_Penawaran='".$item['ID_Penawaran']."'");
            $barang = mysql_fetch_array($get_data);
            $cekkat = substr($barang['ID'],0,3);
              if($cekkat=="BAH")
              {
                $ambilnamat = mysql_query("SELECT * FROM master_bahan_tani WHERE ID_Bahan='{$barang['ID']}'");
                $ambilnama = mysql_fetch_array($ambilnamat);
                $nama=$ambilnama['Nama_Bahan'];
                $path="bahan";
              }
              elseif($cekkat=="HAS")
              {
                $ambilnamat = mysql_query("SELECT * FROM master_hasil_tani WHERE ID_Hasil='{$barang['ID']}'");
                $ambilnama = mysql_fetch_array($ambilnamat);
                $nama=$ambilnama['Nama_Hasil'];
                $path="produk";
              }
              elseif($cekkat=="ALA")
              {
                $ambilnamat = mysql_query("SELECT * FROM master_alat_tani WHERE ID_Alat='{$barang['ID']}'");
                $ambilnama = mysql_fetch_array($ambilnamat);
                $nama=$ambilnama['Nama_Alat'];
                $path="alat";
              };
              $ambilusert = mysql_query("SELECT * FROM master_detail_user WHERE ID_User='{$barang['ID_User']}'");
              $ambiluser = mysql_fetch_array($ambilusert);
              $namauser=$ambiluser['nama'];

              $ambilkurir = mysql_query("SELECT * FROM kurir WHERE kode_kurir='{$item['kode_kurir']}'");
              $kurir = mysql_fetch_array($ambilkurir);

              $ambilalamat = mysql_query("SELECT * FROM detail_alamat WHERE kode_alamat='{$item['kode_alamat']}'");
              $alamat = mysql_fetch_array($ambilalamat);
      ?> 
      <div class="row" style="border:1px solid #666">
        <div style="background-color:#f5f5f5; padding:10px;"><div class="icon-block" >Penjual : <a href=""><?php echo $namauser; ?></a></div></div>
        <div class="col s12 m4" style="margin-bottom:15px">
          <div class="icon-block" >
            <br>
            <h5><?php echo $nama; ?></h5>
            <br>
            <img src="../pictures/items/<?php echo $path?>/<?php echo $barang['Gambar1']?>" class="items center" >
            <p>
              <b>Satuan</b> : &nbsp; <?php echo 'Rp '.number_format($barang['Harga']); ?><br>
              <b>Jumlah item</b> : &nbsp; <?php echo $item['Qty']; ?>
              <br>
            </p>
          </div>
        </div>
        <div class="col s12 m4" style="margin-bottom:15px">
          <div class="icon-block">
            <br><br><br>
            <b>Kurir :</b> <?php echo $kurir['nama_kurir']; ?><br>
            <b>Ongkos Kirim :</b> <?php echo 'Rp '.number_format($kurir['harga']); ?><br><br>
            <b>Alamat Tujuan :</b><br>
            <b><?php echo $alamat['nama_penerima']; ?></b><br>
            <?php echo $alamat['alamat_penerima']; ?><br>
            <?php echo $alamat['kabupaten']; ?>, <?php echo $alamat['kodepos']; ?><br>
            <?php echo $alamat['provinsi']; ?><br>
            <br>
          </div>
        </div>
        <div class="col s12 m4" style="margin-bottom:15px">
          <div class="icon-block">
            <br><br><br>
            <?php 
              $hrgbrg = $barang['Harga'] * $item['Qty'];
              $titip = $hrgbrg + $kurir['harga'];
            ?>
            <b>Harga Barang :</b> <?php echo 'Rp '.number_format($hrgbrg); ?><br>
            <b>Subtotal :</b> <?php echo 'Rp '.number_format($hrgbrg + $kurir['harga']); ?><br>
            <br><br>
            <a href="func/hapusshopcart.php?id=<?php echo $item['ID_Permintaan']; ?>" class="btn red lighten-2 waves-effect waves-light" >Hapus</a>
            <a href="bayar.php?id=<?php echo $item['ID_Permintaan']; ?>&byr=<?php echo $titip; ?>" class="btn right" >Bayar</a>
          </div>
          </div>
            <br>
        </div>

      </div>

      <?php         
      }
      ?> 
    </form>
    </div>
  

  <footer class="page-footer teal darken-4">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Apa itu Dodolantani?</h5>
          <p class="grey-text text-lighten-4">Kami adalah sebuah layanan online yang melayani penjual-belian barang dan bahan yang berhubungan dengan pertanian. Anda dapat melakukan pembelian maupun penjualan. Website ini dibuat dengan tujuan untuk memenuhi syarat tugas akhir.</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">A</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">B</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      By <a class="orange-text text-lighten-3" href="#">72130021</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.js"></script>
  <script src="../js/init.js"></script>
  <script type="text/javascript">
   $(document).ready(function() {
      $(".button-collapse").sideNav();
      $('.dropdown-button').dropdown({
      inDuration: 300,
      outDuration: 225,
      constrainWidth: false, // Does not change width of dropdown to that of the activator
      hover: true, // Activate on hover
      gutter: 0, // Spacing from edge
      belowOrigin: false, // Displays dropdown below the button
      alignment: 'left', // Displays dropdown with edge aligned to the left of button
      stopPropagation: false // Stops event propagation
    });
    });
    function call(){
      $('.chk').prop('checked', true);
    };   
   </script>
  </body>
</html>
