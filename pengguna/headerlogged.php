<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Dodolantani</title>

  <!-- CSS  -->
  <link href="../css/icon.css" rel="stylesheet">
  <link href="../css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="../css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <style type="text/css">
  .nav-wrapper .input-field input[type=search] {
    height: 64px;
  }
  .nav-wrapper input[type="search"]:focus {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
  }
  .input-field label {
    max-height: 64px;
  }
  .nav-wrapper input[type="search"]:focus ~ .label-icon.active {
    position: fixed;
    left: 10px;
  }
  .nav-wrapper input[type="search"]:focus ~ .closed {
    position: fixed;
    right: 10px;
  }
  a.lownav:hover {
    background-color: transparent;
    text-decoration: underline;
  }
  </style>
</head>
<body>
  <nav class="teal nav-extended">
  <div class="teal container">
  <div class="teal nav-wrapper">

      <div class="row" style="margin-bottom:0px">
        <div class="col s3">
          <a href="#" class="brand-logo">Dodolantani</a>
          <a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>
        </div>

        <div class="col s5 hide-on-med-and-down">
          <form>
            <div class="input-field">
              <i class="white-text material-icons prefix">search</i>  
              <input id="search" type="search" required placeholder="Cari...">
            </div>
          </form>
        </div>

        <div class="col s4 hide-on-med-and-down">
          <ul class="right">
              <li><a href="shopcart.php"><i class="material-icons">shopping_cart</i></a></li>
              <li><a href="#" class="dropdown-button" href='#' data-activates='dropdown2'><i class="material-icons right">arrow_drop_down</i> nama</a></li>
            </ul>
          </div>
        </div>

        <ul class="side-nav" id="mobile-menu">
            <li><a href="../index.php">Home</a></li>
            <li><a href="shopcart.php">Shopping Cart</a></li>
            <li><a href="../search.php">Search</a></li>
        </ul>

    </div>
  <div class="nav-content nav-wrapper">
      <ul>
        <li><a class="lownav" href="sass.html">Home</a></li>
        <li><a class="lownav dropdown-button" href='#' data-activates='dropdown1'> &nbsp;  &nbsp;  &nbsp; Produk &nbsp;  &nbsp;  &nbsp; </a></li>
        <li><a class="lownav" href="badges.html">Tentang Kami</a></li>
        <li><a class="lownav" href="badges.html">Kontak</a></li>
      </ul>
  </div>
  </div>
</nav> 
  <ul id='dropdown1' class='dropdown-content'>
    <li><a href="#!">Semua Produk</a></li>
    <li><a href="#!">Alat</a></li>
    <li><a href="#!">Bahan</a></li>
    <li><a href="#!">Produk Hasil</a></li>
  </ul>
  <ul id='dropdown2' class='dropdown-content'>
    <li><a href="#!">Profil</a></li>
    <li><a href="#!">Log Pembelian</a></li>
    <li><a href="#!">Daftar Jual</a></li>
    <li><a href="#!">Logout</a></li>
  </ul>
