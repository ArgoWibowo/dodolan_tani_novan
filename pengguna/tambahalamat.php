<?php
session_start();
if(isset($_SESSION['username'])){
  $un = $_SESSION['username'];
  $tipe = $_SESSION['tipe'];
  include "../headers/headerlogged.php";
}else{
  include "../headers/headerguest2.php";
  header("Location:../index.php");
}
if(isset($_SESSION['username'])){
  $un = $_SESSION['username'];
  $tipe = $_SESSION['tipe'];
  if($tipe!=2)
  {
    header("Location:../admin/home.php");
  }
}else{
  header("Location:../index.php");
}


if(isset($_GET['id'])){
  $id = $_GET['id'];
  $kode = $_GET['kode'];
}else{
  header("Location:../index.php");
}

?>


  <br>
 <div class="container">
    <div class="section"> 

      <!--   Side bar   -->
      <div class="row" style="background-color:#f5f5f5;">
        <div class="col s12 m7">
          <br>
          <div class="icon-block">
            <h4>Alamat Pengiriman Barang</h4>
            <br>
            <form action="func/addalamat.php?id=<?php echo $id; ?>&kode=<?php echo $kode; ?>" method="post">
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Penerima</label>
              </div>
              <div class="input-field col s12 m8">
                <input type="text" name="penerima" required>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Beri Nama</label>
              </div>
              <div class="input-field col s12 m8">
                <select class="dropdown" name="namaalamat">
                  <option value="" disabled selected>Pilih Nama Alamat</option>
                  <option value="Rumah">Rumah</option>
                  <option value="Kos">Kos</option>
                  <option value="Kantor">Kantor</option>>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12">
                <textarea id="alamat" name="alamat" required class="materialize-textarea"></textarea>
                <label for="alamat" class="black-text">Alamat</label>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Kode Pos</label>
              </div>
              <div class="input-field col s12 m8">
                <input type="text" name="kodepos" onkeypress='return event.charCode >= 48 && event.charCode <= 57 || event.charCode == 46' required>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Provinsi</label>
              </div>
              <div class="input-field col s12 m8">
                <select name='prov'>
                <option>Pilih Provinsi</option>
                <?php
                $provinsi=mysql_query("SELECT * from provinsi");
                while($prov=mysql_fetch_array($provinsi))
                {
                ?>
                      <option value="<?php echo $prov['Nama_Provinsi'];?>"><?php echo $prov['Nama_Provinsi'];?></option>
                <?php
                }
                ?>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Kabupaten</label>
              </div>
              <div class="input-field col s12 m8">
                <select name='kab'>
                <option>Pilih Kabupaten</option>
                <?php
                $provinsi=mysql_query("SELECT * from provinsi");
                while($prov=mysql_fetch_array($provinsi))
                {
                ?>
                  <optgroup label="<?php echo $prov['Nama_Provinsi'];?>">
                    <?php
                    $kabupaten=mysql_query("SELECT * from kabupaten WHERE Nama_Provinsi='$prov[Nama_Provinsi]'");
                    while($kab=mysql_fetch_array($kabupaten))
                    {
                    ?>
                      <option value="<?php echo $kab['Nama_Kabupaten'];?>"><?php echo $kab['Nama_Kabupaten'];?></option>
                    <?php  
                    }
                    ?>
                  </optgroup>
                <?php
                }
                ?>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Kecamatan</label>
              </div>
              <div class="input-field col s12 m8">
                <select name='kecamatan'>
                <option>Pilih Kecamatan</option>
                <?php
                $provinsi=mysql_query("SELECT * from provinsi order by Nama_Provinsi");
                while($prov=mysql_fetch_array($provinsi))
                {
                ?>
                <optgroup label="<?php echo $prov['Nama_Provinsi'];?>">
                  <?php
                  $kabupaten=mysql_query("SELECT * from kabupaten WHERE Nama_Provinsi='$prov[Nama_Provinsi]' Order by Nama_Kabupaten");
                  while($kab=mysql_fetch_array($kabupaten))
                  {
                  ?>
                  <optgroup label="<?php echo $kab['Nama_Kabupaten'];?>">
                    <?php
                    $kecamatan=mysql_query("SELECT * from kecamatan WHERE Nama_Provinsi='$prov[Nama_Provinsi]' AND Nama_Kabupaten='$kab[Nama_Kabupaten]' Order by Nama_Kecamatan");
                    while($kec=mysql_fetch_array($kecamatan))
                      {
                      ?>
                      <option value="<?php echo $kec['Nama_Kecamatan'];?>"><?php echo $kec['Nama_Kecamatan'];?></option>
                      <?php
                      }
                      ?>
                  </optgroup>
                  <?php
                  }
                  ?>
                </optgroup>
                <?php
              }
              ?>
          </select>
        </div>
      </div>
            <div class="row">
              <div class="input-field col s12 m4">
                <label class="black-text">Kelurahan Desa</label>
              </div>
              <div class="input-field col s12 m8">
                <select name ='kelurahan_desa'>
                  <option>Pilih Kelurahan Desa</option>
                  <?php
                  $provinsi=mysql_query("SELECT * from provinsi order by Nama_Provinsi");
                  while($prov=mysql_fetch_array($provinsi))
                  {
                  ?>
                  <optgroup label="<?php echo $prov['Nama_Provinsi'];?>">
                    <?php
                    $kabupaten=mysql_query("SELECT * from kabupaten WHERE Nama_Provinsi='$prov[Nama_Provinsi]' Order by Nama_Kabupaten");
                    while($kab=mysql_fetch_array($kabupaten))
                    {
                    ?>
                    <optgroup label="<?php echo $kab['Nama_Kabupaten'];?>">
                      <?php
                      $kecamatan=mysql_query("SELECT * from kecamatan WHERE Nama_Provinsi='$prov[Nama_Provinsi]' AND Nama_Kabupaten='$kab[Nama_Kabupaten]' Order by Nama_Kecamatan");
                      while($kec=mysql_fetch_array($kecamatan))
                      {
                      ?>
                      <optgroup label="<?php echo $kec['Nama_Kecamatan'];?>">
                        <?php
                        $kelurahan_desa=mysql_query("SELECT * from kelurahan_desa WHERE Nama_Provinsi='$prov[Nama_Provinsi]' AND Nama_Kabupaten='$kab[Nama_Kabupaten]' AND Nama_Kecamatan='$kec[Nama_Kecamatan]' Order by Nama_Desa");
                        while($keldes=mysql_fetch_array($kelurahan_desa))
                        {
                        ?>
                        <option value="<?php echo $keldes['Nama_Desa'];?>"><?php echo $keldes['Nama_Desa'];?></option>
                        <?php
                        }
                        ?>
                      </optgroup>
                      <?php
                      }
                      ?>
                    </optgroup>
                    <?php
                    }
                    ?>
                  </optgroup>
                  <?php
                  }
                  ?>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="input-field col s12 m4">
              </div>
              <div class="input-field col s12 m8">
                <input type="submit" value="Tambah alamat" class="btn right">
              </div>
            </div>


            </form><br><br>
          </div>
          <br><br>
        </div>

      <div class="col s12 m5">
              <?php 
                $cek = mysql_query("select (ID_User) from detail_alamat where ID_User='".$un."'");
                if(mysql_num_rows($cek)>0){
              ?> 
          <br><br><b>Daftar Alamat Tersimpan</b>
          <div class="icon-block">
            <ul class="collapsible" data-collapsible="accordion">
              <?php 
                  $hasil = mysql_query("SELECT * FROM detail_alamat WHERE ID_User='$un'");
                  $no = 1;
                  while ($brs = mysql_fetch_array($hasil)){
              ?> 
              <li>
                <div class="collapsible-header"><b><?php echo $no.'. '.$brs['nama_penerima'].', '.$brs['nama_alamat'] ?></b></div>
                <div class="collapsible-body"><span>
                  <?php echo $brs['alamat_penerima'].', '.$brs['kabupaten'].', '.$brs['provinsi'].', '.$brs['kodepos'].'.' ?>
                  <br><a class="right" href="func/delalamat.php?id=<?php echo $id; ?>&kode=<?php echo $brs['kode_alamat']; ?>"> [Hapus]</a><br>
                </span></div>
              </li>
              <?php 
                  $no++; }
              ?> 
            </ul>
          </div>
              <?php 
                }
              ?> 
          <br><br>
        </div>
        <br>
      </div>

    </div>
    </div>
    </div>

  <br><br>

  <footer class="page-footer teal darken-4">
    <div class="container">
      <div class="row">
        <div class="col l6 s12">
          <h5 class="white-text">Apa itu Dodolantani?</h5>
          <p class="grey-text text-lighten-4">Kami adalah sebuah layanan online yang melayani penjual-belian barang dan bahan yang berhubungan dengan pertanian. Anda dapat melakukan pembelian maupun penjualan. Website ini dibuat dengan tujuan untuk memenuhi syarat tugas akhir.</p>


        </div>
        <div class="col l3 s12">
          <h5 class="white-text">A</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
        <div class="col l3 s12">
          <h5 class="white-text">B</h5>
          <ul>
            <li><a class="white-text" href="#!">Link 1</a></li>
            <li><a class="white-text" href="#!">Link 2</a></li>
            <li><a class="white-text" href="#!">Link 3</a></li>
            <li><a class="white-text" href="#!">Link 4</a></li>
          </ul>
        </div>
      </div>
    </div>
    <div class="footer-copyright">
      <div class="container">
      By <a class="orange-text text-lighten-3" href="#">72130021</a>
      </div>
    </div>
  </footer>


  <!--  Scripts-->
  <script src="../js/jquery-2.1.1.min.js"></script>
  <script src="../js/materialize.js"></script>
  <script src="../js/init.js"></script>
  <script type="text/javascript">
    $(document).ready(function() {
      $('select').material_select();
      $('.tooltipped').tooltip({delay: 50});
    });
    </script>
  </body>
</html>
