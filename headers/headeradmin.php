<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Dodolantani</title>

  <!-- CSS  -->
  <link href="../css/icon.css" rel="stylesheet">
  <link href="../css/materialize.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <link href="../css/style.css" type="text/css" rel="stylesheet" media="screen,projection"/>
  <style type="text/css">
  .nav-wrapper .input-field input[type=search] {
    height: 64px;
  }
  .nav-wrapper input[type="search"]:focus {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
  }
  .input-field label {
    max-height: 64px;
  }
  .nav-wrapper input[type="search"]:focus ~ .label-icon.active {
    position: fixed;
    left: 10px;
  }
  .nav-wrapper input[type="search"]:focus ~ .closed {
    position: fixed;
    right: 10px;
  }
  a.lownav:hover {
    background-color: transparent;
    text-decoration: underline;
  }
  </style>
</head>
<body>
  <nav class="teal nav-extended">
  <div class="teal container">
  <div class="teal nav-wrapper">

      <div class="row" style="margin-bottom:0px">
        <div class="col s3">
          <a href="index.php" class="brand-logo">Dodolantani</a>
          <a href="#" data-activates="mobile-menu" class="button-collapse"><i class="material-icons">menu</i></a>
        </div>
    <br>
  <div class="nav-content nav-wrapper">
      <ul>
        <br>
        <li><a class="lownav" href="../admin/cekpembayaran.php">Cek Pembayaran</a></li>
        <li><a class="lownav dropdown-button" href='#' data-activates='dropdown1'> Cek Laporan &nbsp;  </a></li>
        <li><a class="lownav" href="../admin/logout.php">Logout</a></li>
      </ul>
  </div>
  </div>
  
</nav> 
  <ul id='dropdown1' class='dropdown-content'>
    <li><a href="../admin/laporanpembelian.php">Laporan Pembelian</a></li>
    <li><a href="../admin/rekapharian.php">Rekap Harian</a></li>
  </ul>
  